#!/bin/sh

# build.sh - build the dummy-jdk-exclusive debian package.
#
# Copyright (C) 2023, 2025 Daltux <https://daltux.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <https://gnu.org/licenses/>.

start_dir="$(pwd)"
script_dir="$( cd -P "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

base="dummy-jdk-exclusive"
file="$base.equivs"
out_dir="$script_dir/out"

move() {
	for i in "$@" ; do
		mv -vi "${base}_"*"$i" "$out_dir"
		result="$?"
		[ $result -eq 0 ] || return $result
	done
}

if ! command -v equivs-build > /dev/null 2>&1 ; then
	echo '"equivs-build" not found! Please install package "equivs" and try again.'
else
	equivs-build --full "$script_dir/$file"
	ex_code="$?"
	
	if [ $ex_code -ne 0 ]; then 
		echo
		echo 'Error detected.'
		exit $ex_code
	fi
	
	echo
	mkdir -vp "$out_dir" && \
		move .deb .buildinfo .changes .dsc .tar.xz 
	ex_code="$?"
	
	if [ $ex_code -eq 0 ] ; then
		echo
		ls -lh "$out_dir"
	else
		echo
		echo 'Error detected.'
	fi
	
	# shellcheck disable=SC2164
	cd "$start_dir"
	ex_code="$?"
	
	if [ $ex_code -eq 0 ] ; then
		echo 'Done.'
	else
		echo "Error changing directory to $start_dir"
		exit $ex_code
	fi
fi
