# dummy-jdk-deb

This is a minimal [Debian](https://debian.org) package designed to signal to the packaging system that a [Java Development Kit (JDK)](https://en.wikipedia.org/wiki/Java_Development_Kit) is available.

Developers and advanced users of [Java](https://en.wikipedia.org/wiki/Java_\(software_platform\)) often unpack a [Java Virtual Machine (JVM)](https://en.wikipedia.org/wiki/Java_virtual_machine) [tarball](https://en.wikipedia.org/wiki/Tar_\(computing\)) into a directory such as `/opt`. These users typically do not wish to install a Java package from the repositories, which would introduce additional dependencies, solely to satisfy dependencies for other packages.

This package enables the installation of other Debian packages that require Java. The package **`dummy-jdk-exclusive`** conflicts with `java-common` to ensure that only one JDK-related package is installed at a time. Commands such as `java`, `javac`, and others must be available in the system's `PATH`. To simplify this process, create a symbolic link to the preferred JVM directory at `/usr/lib/jvm/default`.

For example:

	ln -s /path/to/jvm /usr/lib/jvm/default

This location is checked by a system-wide shell profile script installed by this package in `/etc/profile.d/`. While the default directory exists, it will be set during the next login:

- The environment variable `JAVA_HOME` will point to `/usr/lib/jvm/default`.
- The subdirectory `bin` will be prepended to the `PATH`.

### Notes:
1. **Compatibility**: This package is specifically designed for **Debian**-based [**GNU**/Linux](https://www.gnu.org/gnu/gnu-linux-faq.html) distributions.
2. **Configuration**: To ensure proper functionality, verify that the symbolic link points to a valid JVM installation.
3. **Environment Variables**: After the next login, the directory `/usr/lib/jvm/default/bin` will be automatically added to the beginning of the `PATH`.

## Installing

Check the repository
[Packages](https://git.disroot.org/daltux/dummy-jdk-deb/packages)
page to obtain the latest released version. Instructions are available for using
APT or direct downloading the file.

## Building

To build the artifact from the source code in this repository, use a tool from package `equivs`:

    equivs-build --full dummy-jdk-exclusive.equivs

## Copyright, licensing and disclaimer

Copyright © 2023, 2025 [Daltux](https://daltux.net)

This program is Free Software: you can redistribute it and/or modify
it under the terms of the [GNU General Public License](LICENSE.md) as
published by the [Free Software Foundation](https://fsf.org), either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
[GNU General Public License](LICENSE.md) for more details.
