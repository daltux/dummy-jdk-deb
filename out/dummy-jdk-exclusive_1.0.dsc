Format: 3.0 (native)
Source: dummy-jdk-exclusive
Binary: dummy-jdk-exclusive
Architecture: all
Version: 1.0
Maintainer: Daltux <https://daltux.net>
Homepage: https://git.disroot.org/daltux/dummy-jdk-deb
Standards-Version: 3.9.2
Build-Depends: debhelper-compat (= 12)
Package-List:
 dummy-jdk-exclusive deb java optional arch=all
Checksums-Sha1:
 49f08a31b7e3b698592c206fd28a76bc79df50bc 2160 dummy-jdk-exclusive_1.0.tar.xz
Checksums-Sha256:
 88e187d4458bc8f68a5a6d0ae403929de99c0cf385599df7f146934544b8b4af 2160 dummy-jdk-exclusive_1.0.tar.xz
Files:
 c35fcf3d9e17148c0fdc14bb518416dd 2160 dummy-jdk-exclusive_1.0.tar.xz
