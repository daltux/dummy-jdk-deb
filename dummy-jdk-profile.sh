# dummy-jdk-profile.sh - when check default JDK directory and, when present, include it in

# This file is part of package 'dummy-jdk-exclusive'

# Copyright (C) 2023, 2025 Daltux <https://daltux.net>
# This is free software. License: GPLv2+

if [ -d /usr/lib/jvm/default/bin ]; then
	export PATH="/usr/lib/jvm/default/bin:$PATH"
	export JAVA_HOME=/usr/lib/jvm/default/
fi
